#Notas sobre o Git
Criação do projeto:

$git init


Adição de arquivo a ser monitorado:

$git add arquivo


Obter informações:

$git status


Criação de uma nova versão:

$git commit -m "informações a serem adicionadas"


Listar as versões:

$git log


Para alterar a versão atual:

$git checkout versão


Para enviar alterações para um servidor remoto:

$git push origin master